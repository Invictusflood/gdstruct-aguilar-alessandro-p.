#include<iostream>

using namespace std;

int computerDigitSum(int x, int sum)
{
	sum = sum + x % 10;
	x = x / 10;
	if (x != 0)
	{
		sum = computerDigitSum(x, sum);
	}
	return sum;
}

void printFibonacci(int currentTerm, int n, int t1, int t2)
{
	if (currentTerm == 1)
	{
		cout << " " << t1;
	}
	else if(currentTerm == 2)
	{
		cout << " " << t2;
	}
	else
	{
		int temp = t1 + t2;
		t1 = t2;
		t2 = temp;
		cout << " " << t2;
	}
	currentTerm++;

	if (currentTerm <= n)
	{
		printFibonacci(currentTerm, n, t1, t2);
	}
}

bool isPrime(int num, int divisor)
{
	int max = num / 2;

	if (num <= 1)
	{
		return false;
	}

	if (divisor <= max)
	{
		if (num % divisor == 0)
		{
			return false;
		}
		else
		{
			divisor++;
			return isPrime(num, divisor);
		}
	}

	return true;
}

void main()
{
	int x, n, num;

	//Sum of Digits
	cout << "Input number to add its digits: ";
	cin >> x;
	cout << computerDigitSum(x, 0) << endl;

	//Fibonacci Sequence till n
	cout << "Until what term of the fibonacci sequence do you want to print?: ";
	cin >> n;
	printFibonacci(1, n, 0, 1);
	cout << endl;

	//Check if Prime
	cout << "Input number to check if prime: ";
	cin >> num;
	if (isPrime(num, 2))
	{
		cout << "Number is Prime" << endl;
	}
	else
	{
		cout << "Number is NOT Prime" << endl;
	}

	system("pause");
}