#include<iostream>

using namespace std;

void inputValues(int *array, int size)
{
	cout << "Input 10 Values" << endl;
	for (int i = 0; i < size; i++)
	{
		cin >> array[i];
	}
}

void sortArray(int *array, int size)
{
	cout << "How would you like it sorted? 1 - Ascending  2 - Descending" << endl;
	int choice;
	cin >> choice;
	cout << endl;
	switch (choice)
	{
	case 1:
		for (int i = 0; i < size - 1; i++)
		{
			for (int j = 0; j < size - i - 1; j++)
			{
				if (array[j] > array[j + 1])
				{
					swap(array[j], array[j + 1]);
				}
			}
		}
		break;
	case 2:
		for (int i = 0; i < size - 1; i++)
		{
			for (int j = 0; j < size - i - 1; j++)
			{
				if (array[j] < array[j + 1])
				{
					swap(array[j], array[j + 1]);
				}
			}
		}
		break;
	}
}

void printArray(int* array, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << array[i] << endl;
	}
}

void valueSearch(int* array, int size)
{
	cout << "Input a value you want to find" << endl;
	int input;
	cin >> input;
	cout << endl;
	for (int i = 0; i < size; i++)
	{
		if (input == array[i])
		{
			cout << array[i] << endl;
			cout << i + 1 << " steps to find input" << endl;
			break;
		}
	}
	cout << "Value not found" << endl;
}

void main()
{
	const int size = 10;
	int array[size];
	inputValues(array, size);
	sortArray(array, size);
	printArray(array, size);
	valueSearch(array, size);
	system("pause");
}