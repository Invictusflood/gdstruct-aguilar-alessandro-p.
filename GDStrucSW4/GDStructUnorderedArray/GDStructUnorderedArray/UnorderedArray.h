#pragma once

#include <assert.h>

using namespace std;

template<class T>
class UnorderedArray
{
private:
	T* mArray;
	int mMaxSize; //the size we are going to declare in initialization
	int mGrowSize; //this is the number of elements to be added everytime we add elements
	int mNumElements; //current size of array

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp, mArray, sizeof(T) * mMaxSize);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size) //if size has value
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			// int = 4/8 bytes
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0); //In-Line if statment, if condition is true, first value is assign and if false, second
		}
	}

	~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual int linearSearch()
	{
		T value;
		assert(mArray != NULL);

		cout << "Input element you want to find the index of: " << endl;
		cin >> value;
		for (int i = 0; i < getSize(); i++)
		{
			if (value == mArray[i])
			{
				cout << "Index: " << endl;
				return i;
			}
		}
		cout << "Not Found: " << endl;
		return -1;
	}
};