#include <string>
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

void main()
{
	UnorderedArray<int> grades(5);

	grades.push(69);
	grades.push(420);

	cout << "Array Contents: " << endl;
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	cout << grades.linearSearch() << endl;

	system("pause");
}