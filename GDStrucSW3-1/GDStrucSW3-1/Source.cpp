#include<iostream>
#include<time.h>
#include<string>
#include<stdlib.h>

using namespace std;

void nameGuild(string& guildName)
{
	cout << "Guild Name:";
	cin >> guildName;
	system("pause");
	system("CLS");
}

void inputInitalSize(int& size)
{
	cout << "Initial Member Size:";
	cin >> size;
	system("pause");
	system("CLS");
}

void displayMembers(string* guildMembers, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << i << ": " <<guildMembers[i] << endl;
	}
}

void nameInitialMembers(string* guildMembers, int size)
{
	cout << "Please Name Inital Members" << endl;
	for (int i = 0; i < size; i++)
	{
		cout << i << ": ";
		cin >> guildMembers[i];
	}
	system("CLS");
}

void renameMember(string* guildMembers, int size)
{
	int i;
	cout << "Member Position(Starting from 0): " << endl;
	cin >> i;
	if (i < size)
	{
		cout << guildMembers[i] << " selected" << endl;
		cout << "Rename: ";
		cin >> guildMembers[i];
	}
	else
	{
		cout << "Position does not exist" << endl;
	}
}

void resizeGuild(string* &guildMembers, int& size, int sizeChange)
{
	string* tempArrayPtr = guildMembers;
	size += sizeChange;
	guildMembers = new string[size];
	int tempPosition = 0;
	if (sizeChange == -1)
	{
		for (int i = 0; i < size; i++)
		{
			if (tempArrayPtr[i] == "")
			{
				tempPosition++;
			}
			guildMembers[i] = tempArrayPtr[tempPosition];
			tempPosition++;
		}
	}
	else if (sizeChange == 1)
	{
		for (int i = 0; i < size; i++)
		{
			if (i != size - 1)
			{
				guildMembers[i] = tempArrayPtr[i];
			}
		}
	}
	delete[] tempArrayPtr;
}

void addMember(string* &guildMembers, int& size)
{
	int sizeChange = 1;
	string newMember = "";
	resizeGuild(guildMembers, size, sizeChange);
	cout << "Member Name: ";
	while (newMember == "")
	{
		cin >> newMember;
	}
	guildMembers[size - 1] = newMember;
}

void removeMember(string* &guildMembers, int& size)
{
	int sizeChange = -1;
	int i;
	cout << "Member Position(Starting from 0): ";
	cin >> i;
	if (i < size)
	{
		guildMembers[i] = "";
		resizeGuild(guildMembers, size, sizeChange);
	}
	else
	{
		cout << "Position does not exist" << endl;
	}
}

void memberConfiguration(string* &guildMembers, int& size)
{
	int choice = 0;
	while (choice != 4)
	{
		cout << "Member Configuration" << endl;
		cout << "=======================================" << endl;
		cout << "0- Display Members, 1 - Rename, 2 - Add, 3 - Remove, 4 - Quit" << endl;
		cout << "Choice: ";
		cin >> choice;
		cout << endl;

		if (choice == 0 || choice == 1 || choice == 3)
		{
			if (size == 0)
			{
				choice = -1;
			}
		}

		switch (choice)
		{
		case -1:
			cout << "Error: There are no member in the guild" << endl;
			break;
		case 0:
			displayMembers(guildMembers, size);
			break;
		case 1:
			renameMember(guildMembers, size);
			break;
		case 2:
			addMember(guildMembers, size);
			break;
		case 3:
			removeMember(guildMembers, size);
			break;
		case 4:
			cout << "Quitting..." << endl;
			break;
		default:
			cout << "Please Input one of the choices" << endl;
			break;
		}
		cout << endl;
		system("pause");
		system("CLS");
	}
}

void main()
{
	srand(time(NULL));
	string guildName;
	nameGuild(guildName);

	int size;
	inputInitalSize(size);

	string* guildMembers = new string [size];
	nameInitialMembers(guildMembers,size);

	memberConfiguration(guildMembers, size);

	delete[] guildMembers;
}