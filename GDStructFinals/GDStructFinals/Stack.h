#pragma once
#include "UnorderedArray.h"
template<class T>
class Stack
{
public:
	Stack(int size)
	{
		if (size)
		{
			mContainer = new UnorderedArray<T>(size);
		}
	}

	~Stack()
	{
		if (mContainer != NULL)
		{
			delete[] mContainer;
			mContainer = NULL;
		}
	}

	void push(T value)
	{
		mContainer->pushFront(value);
	}

	void pop()
	{
		mContainer->popFront();
	}

	void printAndClear()
	{
		int outputOrder = 1;
		cout << "Stack elements:" << endl;
		while (mContainer->getSize() > 0)
		{
			cout <<outputOrder << ". "<< getTop() << endl;
			pop();
			outputOrder++;
		}
	}

	const T& getTop()
	{
		return mContainer->getTop();
	}

	bool isNotEmpty()
	{
		if (mContainer->getSize() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private:
	UnorderedArray<T>* mContainer;
};