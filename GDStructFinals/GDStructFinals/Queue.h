#pragma once
#include "UnorderedArray.h"
template<class T>
class Queue
{
public:
	Queue(int size)
	{
		if (size)
		{
			mContainer = new UnorderedArray<T>(size);
		}
	}

	~Queue()
	{
		if (mContainer != NULL)
		{
			delete[] mContainer;
			mContainer = NULL;
		}
	}

	void push(T value)
	{
		mContainer->push(value);
	}

	void pop()
	{
		mContainer->popFront();
	}

	void printAndClear()
	{
		int outputOrder = 1;
		cout << "Queue elements:" << endl;
		while (mContainer->getSize() > 0)
		{
			cout <<outputOrder << ". "<< getTop() << endl;
			pop();
			outputOrder++;
		}
	}

	const T& getTop()
	{
		return mContainer->getTop();
	}

	bool isNotEmpty()
	{
		if (mContainer->getSize() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private:
	UnorderedArray<T>* mContainer;
};