#include <iostream>
#include <stdlib.h>
#include "Stack.h"
#include "Queue.h"

using namespace std;
int selectChoice()
{
	int choice;
	cout << "What do you want to do?" << endl;
	cout << "1 - Push elements" << endl;
	cout << "2 - Pop elements" << endl;
	cout << "3 - Print everything then empty set" << endl;
	cin >> choice;
	cout << endl;
	return choice;
}

void printTop(Queue<int>& queue ,Stack<int>& stack)
{
	cout << "Top element of sets:" << endl;
	cout << "Queue: " << queue.getTop() << endl;
	cout << "Stack: " << stack.getTop() << endl;
}

void pushElement(Queue<int>& queue, Stack<int>& stack)
{
	int input;
	cout << "Enter a number: ";
	cin >> input;
	stack.push(input);
	queue.push(input);
	printTop(queue, stack);
}

void popElement(Queue<int>& queue, Stack<int>& stack)
{
	stack.pop();
	queue.pop();
	cout << "You have popped the front elements" << endl;
	if (queue.isNotEmpty() && stack.isNotEmpty())
	{
		printTop(queue, stack);
	}
}

void printAndClear(Queue<int>& queue, Stack<int>& stack)
{
	queue.printAndClear();
	cout << endl;
	stack.printAndClear();
}

void main()
{
	int size;
	cout << "Enter size for element set: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	while (true)
	{
		switch (selectChoice())
		{
		case 1:
			pushElement(queue, stack);
			break;
		case 2:
			popElement(queue, stack);
			break;
		case 3:
			printAndClear(queue, stack);
			break;
		default:
			cout << "Please choose one of the options" << endl;
			break;
		}
		system("pause");
		system("CLS");
	}
}